package com.yawn.spock

import com.yawn.spock.service.CalculateService
import groovy.sql.Sql
import spock.lang.Shared
import spock.lang.Specification

/**
 * spock 数据驱动测试
 * @author yawn
 *     2019/6/10 9:43
 */
class DataDrivenSpec extends Specification {

    @Shared
    CalculateService calculateService
    @Shared
    Sql sql = Sql.newInstance("jdbc:mysql://localhost:3306/test?serverTimezone=UTC&characterEncoding=UTF-8&useUnicode=true", "root", "root", "com.mysql.cj.jdbc.Driver")

    // 初始化
    def setupSpec() {
        calculateService = new CalculateService()
    }

    def "没有参数的 verifyAll"() {
        when:
        int z1 = calculateService.plus(1, 1)
        int z2 = calculateService.plus(1, 2)
        int z3 = calculateService.plus(1, 3)

        then:
        verifyAll {
            z1 == 1
            z2 == 3
            z3 == 3
        }
    }


    /**
     * 数据管道
     */
    def "computing the maximum of two numbers"() {
        expect:
        Math.max(a, b) == c

        where:
        a << [5, 3] // 执行两次测试，值依次为5，3，下面类似
        b << [1, 9] // << is a Groovy shorthand for List.add()
        c << [5, 9]
    }

    /**
     * sql 多变量的数据管道
     */
    def "sql 多变量的数据管道"() {

        expect:
        Math.max(a, b) == c

        where:
        [a, b, c, _] << sql.rows("select * from test_case")
        // [[a, b, c], [a, c]]
    }

    /**
     * 数据表
     */
    def "test1"() {
        given: "准备mock数据"

        expect: "测试方法"
        z == calculateService.plus(x, y)

        where: "校验结果"
        x | y || z
        1 | 0 || 1
        2 | 1 || 3

    }

}
