package com.yawn.spock

import spock.lang.Specification
import static spock.util.matcher.HamcrestSupport.expect
import static spock.util.matcher.HamcrestMatchers.closeTo


/**
 *
 * @author yawn
 *     2019/8/28 16:31
 *
 */
class HamcrestSupportSpec extends Specification {


    def "test for closeTo()"() {
        given:
        int a = 101256
        when:
        int b = a / 10
        then:
//        HamcrestSupport.expect(b, HamcrestMatchers.closeTo(10000, 200))
//        expect(b, closeTo(10000, 200))
        expect b, closeTo(10000, 200)
    }

}
