package com.yawn.spock

import com.yawn.spock.util.MyMath
import spock.lang.*

/**
 * 测试用例（test case）
 * @author yawn
 */
class TestCaseSpec extends Specification {


    @Unroll
    def "#a 的绝对值是 #b"() {
        expect:
        MyMath.abs(a) == b

        where:
        a  | b
        -1 | 1
        3  | 3
        0  | 0
    }

    def "test my sum"() {
        expect:
        MyMath.mySum(a, b, c) == sum

        where:
        a | b | c || sum
        -1| -1| -1|| 0
        -1| 11|111|| 2
        1 | -1|111|| 2
        1 | 11| -1|| 2
    }

}
