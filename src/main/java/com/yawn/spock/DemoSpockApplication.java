package com.yawn.spock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpockApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpockApplication.class, args);
    }

}
