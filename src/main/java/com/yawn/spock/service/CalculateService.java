package com.yawn.spock.service;

import org.springframework.stereotype.Service;

/**
 * @author yawn
 * 2019/6/10 9:46
 */
@Service
public class CalculateService implements CalculateInterface {

    @Override
    public int plus(int x, int y) {
        return x + y;
    }


    @Override
    public int plusPlus(int x) {
        return ++x;
    }


    @Override
    public int minus(int x, int y) {
        return x - y - y + y;
    }
}
